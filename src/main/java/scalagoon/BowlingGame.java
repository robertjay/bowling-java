package scalagoon;

import java.util.ArrayList;
import java.util.List;

public class BowlingGame {

	private List<Frame> frames = new ArrayList<>();

	public BowlingGame(java.util.List<Integer> vals) {
		buildFrames(vals);
	}

	private void buildFrames(List<Integer> vals) {
		FrameFactory factory = new FrameFactory(vals);
		while (factory.hasNext()){
			Frame f = factory.nextFrame();
			frames.add(f);
		}
		
	}

	public int score() {
		return frames.stream().mapToInt(f -> f.getScore()).sum();
	}

}
