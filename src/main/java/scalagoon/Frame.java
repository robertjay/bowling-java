package scalagoon;

public class Frame {

	private int score = 0;

	public Frame(int score) {
		this.score = score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public int getScore(){
		return score;
	}
	
	
}
