package scalagoon;

import java.util.LinkedList;
import java.util.List;

public class FrameFactory {

	private static final int MAX_FRAME_SCORE = 10;
	private static final int EXPECTED_FRAMES = 10;

	private List<Integer> remainingVals;
	private int frameCount = 0;
	
	public FrameFactory(List<Integer> vals) {
		remainingVals = new LinkedList<>(vals);
	}
	
	public boolean hasNext(){
		return frameCount < EXPECTED_FRAMES && !remainingVals.isEmpty();
	}

	public Frame nextFrame(){
		if (!hasNext()){
			return null;
		}
		frameCount++;
		int score = pollValue();
		if (score == MAX_FRAME_SCORE){
			return strike();
		}
		score += pollValue();
		if (score == MAX_FRAME_SCORE){
			return spare();
		}
		return frame(score, 0);
	}

	private Frame strike() {
		return frame(MAX_FRAME_SCORE, 2);
	}
	
	private Frame spare() {
		return frame(MAX_FRAME_SCORE, 1);
	}
	
	private Frame frame(int score, int lookAhead){
		for (int i=0; i<lookAhead &&  i < remainingVals.size(); i++){
			score += remainingVals.get(i);
		}
		return new Frame(score);
	}
	
	private int pollValue(){
		if (remainingVals.isEmpty()){
			return 0;
		}
		return remainingVals.remove(0);
	}
	
	
}
